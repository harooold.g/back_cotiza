require("dotenv").config();
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
var fs = require("fs");
var cors = require("cors");

const mongoose = require("mongoose");
//mongodb+srv://hcohen:harold@cluster0.y93ce.mongodb.net/cotizaciones?retryWrites=true&w=majority
//mongodb://127.0.0.1/cotizaciones
mongoose
  .connect(
    "mongodb+srv://hcohen:harold@cluster0.y93ce.mongodb.net/cotizaciones?retryWrites=true&w=majority",
    {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    }
  )
  .then(() => console.log("Mongo Db Connected"))
  .catch((err) => {
    console.log(Error, err.message);
  });

global.refreshTokens = [];
global.utils = require("./helpers/utils");
global.PDFDocument = require("pdfkit");
global.fs = require("fs");

var usuariosRouter = require("./routes/usuarios");
var clientesRouter = require("./routes/clientes");
var categoriasRouter = require("./routes/categorias");
var productosRouter = require("./routes/productos");
var serviciosRouter = require("./routes/servicios");
var cotizacionesRouter = require("./routes/cotizaciones");
var authRouter = require("./routes/auth");
var refreshRouter = require("./routes/refresh");

//var indexRouter = require('./routes/index');
//var usersRouter = require('./routes/users');

var app = express();

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use(
  cors({
    origin: "*",
    methods: ["GET", "POST", "PUT", "DELETE", "OPTIONS"],
    allowedHeaders: ["Content-Type", "Authorization"],
    credentials: true,
  })
);

app.set("view engine", "ejs");
app.use(
  express.static("public", {
    setHeaders: (res, filepath) =>
      res.set(
        "Content-Disposition",
        `attachment; filename="pdf-express-${path.basename(filepath)}"`
      ),
  })
);

app.use("/auth", authRouter);

app.use("/api", utils.authenticateToken);
app.use("/refresh", refreshRouter);

app.use("/api/usuarios", usuariosRouter);
app.use("/api/clientes", clientesRouter);
app.use("/api/categorias", categoriasRouter);
app.use("/api/productos", productosRouter);
app.use("/api/servicios", serviciosRouter);
app.use("/api/cotizaciones", cotizacionesRouter);

app.use("/usuarios", usuariosRouter);
app.use("/clientes", clientesRouter);
app.use("/categorias", categoriasRouter);
app.use("/productos", productosRouter);
app.use("/servicios", serviciosRouter);
app.use("/cotizaciones", cotizacionesRouter);
module.exports = app;
