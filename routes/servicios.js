var express = require("express");
var router = express.Router();
const serviciosController = require("../controllers/servicios");

/* GET home page. */
router.post("/", function (req, res, next) {
  const categoria_id = req.body.categoria_id;
  const nombre = req.body.nombre;
  const descripcion = req.body.descripcion;
  const precio = req.body.precio;
  const moneda = req.body.moneda;
  const unidad = req.body.unidad;
  serviciosController
    .store(categoria_id, nombre, descripcion, precio, moneda, unidad)
    .then(
      (success) => {
        res.json({
          message: null,
          data: success,
        });
      },
      (error) => {
        res.status(400).json({
          message: error.message,
          data: null,
        });
      }
    );
});

router.get("/", function (req, res, next) {
  serviciosController.list().then(
    (success) => {
      res.json({
        message: null,
        data: success,
      });
    },
    (error) => {
      res.status(400).json({
        message: error.message,
        data: null,
      });
    }
  );
});

router.get("/:servicio_id/show", function (req, res, next) {
  serviciosController.show(req.params.servicio_id).then(
    (success) => {
      res.json({
        message: null,
        data: success,
      });
    },
    (error) => {
      res.status(400).json({
        message: error.message,
        data: null,
      });
    }
  );
});

router.get("/categorias/:categoria_id", function (req, res, next) {
  serviciosController.listByCategoria(req.params.categoria_id).then(
    (success) => {
      res.json({
        message: null,
        data: success,
      });
    },
    (error) => {
      res.status(400).json({
        message: error.message,
        data: null,
      });
    }
  );
});

router.delete("/:servicio_id", function (req, res, next) {
  serviciosController.delete(req.params.servicio_id).then(
    (success) => {
      res.json({
        message: null,
        data: success,
      });
    },
    (error) => {
      res.status(400).json({
        message: error.message,
        data: null,
      });
    }
  );
});

router.put("/", function (req, res, next) {
  const servicio_id = req.body._id;
  const categoria_id = req.body.categoria_id;
  const nombre = req.body.nombre;
  const descripcion = req.body.descripcion;
  const precio = req.body.precio;
  const moneda = req.body.moneda;
  const unidad = req.body.unidad;
  serviciosController
    .update(
      servicio_id,
      categoria_id,
      nombre,
      descripcion,
      precio,
      moneda,
      unidad
    )
    .then(
      (success) => {
        res.json({
          message: null,
          data: success,
        });
      },
      (error) => {
        res.status(400).json({
          message: error.message,
          data: null,
        });
      }
    );
});

module.exports = router;
