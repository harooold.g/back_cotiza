var express = require('express');
var router = express.Router();

const authController = require('../controllers/auth');
const usuariosController = require('../controllers/usuarios');
const auth = require("http-auth");

const authConnect = require("http-auth-connect");


const basic = auth.basic({ realm: 'AM' }, (username, password, callback) => {
    authController.login(username, password).then(
        (row) => {
            callback(true);
        },
        (error) => {
            console.log(error);
            callback(false);
        }
    )
});

/* GET home page. */
router.post('/usuarios', authConnect(basic), function(req, res) {
    const email = req.user;
    console.log("AAA");
    authController.afterlogin(email).then(
        (success) => {
            console.log(success);
            res.cookie('rftk', success.refreshToken, {
                secure: false, // set to true if your using https
                httpOnly: true, // son inaccesibles por el sitio, solo se envía al servidor
                expires: new Date(Date.now() + 3 * 24 * 60 * 60 * 1000)
            })
            refreshTokens.push(success.refreshToken);
            delete success.refreshToken

            res.json({
                message: null,
                data: success
            })
        },
        (error) => {
            res.status(400).json({
                message: error.message,
                data: null
            })
        }
    )
});

router.delete('/logout', function(req, res, next) {

    refreshTokens = refreshTokens.filter(token => token !== req.cookies.rftk)
    res.cookie('rftk', '', {
        secure: false,
        httpOnly: true,
        expires: new Date(Date.now()),
    })
    res.sendStatus(204);
});

router.post('/usuarios-login', function(req, res) {
    const email = req.body.email;
    const password = req.body.password;
    usuariosController.login(email, password).then(
        (success) => {
            refreshTokens.push(success.refreshToken);
            res.json({
                message: null,
                data: success
            })
        },
        (error) => {
            res.status(400).json({
                message: error.message,
                data: null
            })
        }
    )
});

module.exports = router;