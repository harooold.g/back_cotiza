var express = require("express");
var router = express.Router();
var path = require("path");

const cotizacionesController = require("../controllers/cotizaciones");

const dirPath = path.join(__dirname, "../public/pdf");
const files = fs.readdirSync(dirPath).map((name) => {
  return {
    name: path.basename(name, ".pdf"),
    url: `/pdf/${name}`,
  };
});

router.get("/", function (req, res, next) {
  cotizacionesController.list().then(
    (success) => {
      res.json({
        message: null,
        data: success,
      });
    },
    (error) => {
      res.status(400).json({
        message: error.message,
        data: null,
      });
    }
  );
});

router.post("/", function (req, res) {
  console.log(req.body);
  const cliente = req.body.cliente;
  const direccion = req.body.direccion;
  const email = req.body.email;
  const telefono = req.body.telefono;
  const numero = req.body.numero;
  const trabajo = req.body.trabajo;
  const orden = req.body.orden;

  cotizacionesController
    .store(cliente, direccion, email, telefono, numero, trabajo, orden)
    .then(
      (success) => {
        res.json({
          message: null,
          data: success,
        });
      },
      (error) => {
        res.status(400).json({
          message: error.message,
          data: null,
        });
      }
    );
});

router.get("/:cotizacion_id/show", function (req, res, next) {
  cotizacionesController.show(req.params.cotizacion_id).then(
    (success) => {
      res.json({
        message: null,
        data: success,
      });
    },
    (error) => {
      res.status(400).json({
        message: error.message,
        data: null,
      });
    }
  );
});

router.delete("/:cotizacion_id", function (req, res) {
  cotizacionesController.delete(req.params.cotizacion_id).then(
    (success) => {
      res.json({
        message: null,
        data: success,
      });
    },
    (error) => {
      res.status(400).json({
        message: error.message,
        data: null,
      });
    }
  );
});

router.put("/", function (req, res, next) {
  console.log(req.body);
  const cotizacion_id = req.body._id;
  const cliente = req.body.cliente;
  const direccion = req.body.direccion;
  const email = req.body.email;
  const telefono = req.body.telefono;
  const numero = req.body.numero;
  const trabajo = req.body.trabajo;
  const orden = req.body.orden;

  cotizacionesController
    .update(
      cotizacion_id,
      cliente,
      direccion,
      email,
      telefono,
      numero,
      trabajo,
      orden
    )
    .then(
      (success) => {
        res.json({
          message: null,
          data: success,
        });
      },
      (error) => {
        res.status(400).json({
          message: error.message,
          data: null,
        });
      }
    );
});

router.get("/pdf/:name", function (req, res) {
  // const file = files.find((f) => f.name === req.params.name);
  // res.render("index", { files, file });
  res.download(`./public/pdf/${req.params.name}.pdf`);
});

module.exports = router;
