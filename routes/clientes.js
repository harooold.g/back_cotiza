var express = require("express");
var router = express.Router();
const clientesController = require("../controllers/clientes");

/* GET home page. */
router.post("/", function (req, res) {
  console.log(req.body);

  const nombres = req.body.nombres;
  const apellidos = req.body.apellidos;
  const direccion = null;
  const email = null;
  const telefono = null;
  const ruc = null;

  if (req.body.ruc && req.body.ruc != "") ruc = req.body.ruc;
  if (req.body.email && req.body.email != "") email = req.body.email;

  if (req.body.telefono) telefono = req.body.telefono;
  if (req.body.direccion) direccion = req.body.direccion;

  clientesController
    .store(nombres, apellidos, ruc, email, direccion, telefono)
    .then(
      (success) => {
        res.json({
          message: null,
          data: success,
        });
      },
      (error) => {
        res.status(400).json({
          message: error.message,
          data: null,
        });
      }
    );
});

router.get("/", function (req, res) {
  clientesController.list().then(
    (success) => {
      res.json({
        message: null,
        data: success,
      });
    },
    (error) => {
      res.status(400).json({
        message: error.message,
        data: null,
      });
    }
  );
});

router.get("/:cliente_id/show", function (req, res) {
  clientesController.show(req.params.cliente_id).then(
    (success) => {
      res.json({
        message: null,
        data: success,
      });
    },
    (error) => {
      res.status(400).json({
        message: error.message,
        data: null,
      });
    }
  );
});

router.delete("/:cliente_id", function (req, res) {
  clientesController.delete(req.params.cliente_id).then(
    (success) => {
      res.json({
        message: null,
        data: success,
      });
    },
    (error) => {
      res.status(400).json({
        message: error.message,
        data: null,
      });
    }
  );
});

router.put("/", function (req, res) {
  const cliente_id = req.body.cliente_id;
  const nombres = req.body.nombres;
  const apellidos = req.body.apellidos;
  const direccion = null;
  const email = null;
  const telefono = null;
  const ruc = null;
  if (req.body.ruc) ruc = req.body.ruc;
  if (req.body.email) email = req.body.email;
  if (req.body.telefono) telefono = req.body.telefono;
  if (req.body.direccion) direccion = req.body.direccion;

  clientesController
    .update(cliente_id, nombres, apellidos, ruc, email, direccion, telefono)
    .then(
      (success) => {
        res.json({
          message: null,
          data: success,
        });
      },
      (error) => {
        res.status(400).json({
          message: error.message,
          data: null,
        });
      }
    );
});

module.exports = router;
