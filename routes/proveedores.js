var express = require('express');
var router = express.Router();
const categoriasController = require('../controllers/categorias');

/* GET home page. */
router.post('/', function(req, res, next) {
    res.render('index', { title: 'Express' });
});

router.get('/', function(req, res, next) {
    res.render('index', { title: 'Express' });
});

router.get('/:categoria_id/show', function(req, res, next) {
    res.render('index', { title: 'Express' });
});

router.delete('/:categoria_id', function(req, res, next) {
    res.render('index', { title: 'Express' });
});

router.put('/', function(req, res, next) {
    res.render('index', { title: 'Express' });
});
module.exports = router;