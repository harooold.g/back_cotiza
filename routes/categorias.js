var express = require("express");
var router = express.Router();
const categoriasController = require("../controllers/categorias");

/* GET home page. */
router.post("/", function (req, res, next) {
  const nombre = req.body.nombre;
  const descripcion = req.body.descripcion;

  categoriasController.store(nombre, descripcion).then(
    (success) => {
      res.json({
        message: null,
        data: success,
      });
    },
    (error) => {
      res.status(400).json({
        message: error.message,
        data: null,
      });
    }
  );
});

router.get("/", function (req, res, next) {
  categoriasController.list().then(
    (success) => {
      console.log(success),
        res.json({
          message: null,
          data: success,
        });
    },
    (error) => {
      res.status(400).json({
        message: error.message,
        data: null,
      });
    }
  );
});

router.get("/:categoria_id/show", function (req, res, next) {
  categoriasController.show(req.params.categoria_id).then(
    (success) => {
      res.json({
        message: null,
        data: success,
      });
    },
    (error) => {
      res.status(400).json({
        message: error.message,
        data: null,
      });
    }
  );
});

router.delete("/:categoria_id", function (req, res, next) {
  categoriasController.delete(req.params.categoria_id).then(
    (success) => {
      res.json({
        message: null,
        data: success,
      });
    },
    (error) => {
      res.status(400).json({
        message: error.message,
        data: null,
      });
    }
  );
});

router.put("/", function (req, res, next) {
  const categoria_id = req.body._id;
  const nombre = req.body.nombre;
  const descripcion = req.body.descripcion;

  categoriasController.update(categoria_id, nombre, descripcion).then(
    (success) => {
      res.json({
        message: null,
        data: success,
      });
    },
    (error) => {
      res.status(400).json({
        message: error.message,
        data: null,
      });
    }
  );
});
module.exports = router;
