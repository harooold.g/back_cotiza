var express = require("express");
var router = express.Router();
const productosController = require("../controllers/productos");

/* GET home page. */
router.post("/", function (req, res, next) {
  const categoria_id = req.body.categoria_id;
  const nombre = req.body.nombre;
  const descripcion = req.body.descripcion;
  const precio = req.body.precio;
  const moneda = req.body.moneda;
  const unidad = req.body.unidad;

  productosController
    .store(categoria_id, nombre, descripcion, precio, moneda, unidad)
    .then(
      (success) => {
        res.json({
          message: null,
          data: success,
        });
      },
      (error) => {
        res.status(400).json({
          message: error.message,
          data: null,
        });
      }
    );
});

router.get("/", function (req, res) {
  productosController.list().then(
    (success) => {
      res.json({
        message: null,
        data: success,
      });
    },
    (error) => {
      res.status(400).json({
        message: error.message,
        data: null,
      });
    }
  );
});

router.get("/:producto_id/show", function (req, res) {
  productosController.show(req.params.producto_id).then(
    (success) => {
      res.json({
        message: null,
        data: success,
      });
    },
    (error) => {
      res.status(400).json({
        message: error.message,
        data: null,
      });
    }
  );
});

router.get("/categorias/:categoria_id", function (req, res) {
  productosController.listByCategoria(req.params.categoria_id).then(
    (success) => {
      res.json({
        message: null,
        data: success,
      });
    },
    (error) => {
      res.status(400).json({
        message: error.message,
        data: null,
      });
    }
  );
});

router.delete("/:producto_id", function (req, res) {
  productosController.delete(req.params.producto_id).then(
    (success) => {
      res.json({
        message: null,
        data: success,
      });
    },
    (error) => {
      res.status(400).json({
        message: error.message,
        data: null,
      });
    }
  );
});

router.put("/", function (req, res, next) {
  const producto_id = req.body._id;
  const categoria_id = req.body.categoria_id;
  const nombre = req.body.nombre;
  const descripcion = req.body.descripcion;
  const precio = req.body.precio;
  const moneda = req.body.moneda;
  const unidad = req.body.unidad;

  productosController
    .update(
      producto_id,
      categoria_id,
      nombre,
      descripcion,
      precio,
      moneda,
      unidad
    )
    .then(
      (success) => {
        res.json({
          message: null,
          data: success,
        });
      },
      (error) => {
        res.status(400).json({
          message: error.message,
          data: null,
        });
      }
    );
});

module.exports = router;
