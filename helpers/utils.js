const crypto = require("crypto");
const jwt = require("jsonwebtoken");

const utils = {
  generateAccessToken(data) {
    const payload = data;
    const secret = process.env.JWT_SECRET;
    const options = { expiresIn: "1d" };
    const token = jwt.sign(payload, secret, options);
    return token;
  },

  validateToken(token) {
    return new Promise((resolve, reject) => {
      jwt.verify(token, process.env.JWT_SECRET, (err, data) => {
        if (err) {
          reject(false);
        } else {
          resolve(data);
        }
      });
    });
  },
  generateRefreshToken(data) {
    const payload = data;
    const secret = process.env.JWT_REFRESH;
    const options = { expiresIn: "3d" };
    const rtoken = jwt.sign(payload, secret, options);
    return rtoken;
  },

  validateRefreshToken(rftk) {
    return new Promise((resolve, reject) => {
      let verifyRT = refreshTokens.indexOf(rftk);

      if (verifyRT != -1) {
        jwt.verify(rftk, process.env.JWT_REFRESH, (err, data) => {
          if (err) {
            refreshTokens = refreshTokens.filter((token) => token !== rftk);
            reject(false);
          } else {
            resolve(data);
          }
        });
      } else {
        reject(false);
      }
    });
  },

  authenticateToken(req, res, next) {
    const authHeader = req.headers.authorization;
    const token = authHeader && authHeader.split(" ")[1];
    if (!token) return res.sendStatus(401);
    console.log(token);

    utils.validateToken(token).then(
      (result) => {
        next();
      },
      (err) => {
        return res.sendStatus(401);
      }
    );
  },

  encryp(texto) {
    console.log(texto);

    const secret = process.env.CRYPTO_SECRET;
    const encrypted = crypto
      .createHmac("sha256", secret)
      .update(texto)
      .digest("hex");

    return encrypted;
  },
};
module.exports = utils;
