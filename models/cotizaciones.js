const mongoose = require("mongoose");

const cotizaciones = new mongoose.Schema({
  cliente: { type: String, required: true },
  direccion: { type: String, required: true },
  email: { type: String },
  telefono: { type: String },
  numero: { type: Number, required: true },
  trabajo: { type: String, required: true },
  total: { type: Number },
  fecha_registro: { type: Date, default: new Date() },
  orden: [
    {
      id: { type: String, require: true },
      tipo: { type: String, require: true },
      nombre: { type: String, require: true },
      categoria: { type: String, require: true },
      descripcion: { type: String, require: true },
      unidad: { type: String, required: true },
      precio: { type: Number, require: true },
      moneda: { type: String, require: true },
      cantidad: { type: Number, require: true },
      parcial: { type: Number, require: true },
    },
  ],
  estado: { type: String, default: "ENVIADO" },
});

module.exports = mongoose.model("cotizaciones", cotizaciones);
