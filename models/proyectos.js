const mongoose = require('mongoose');

const proyectos = new mongoose.Schema({
    cotizacion_id: { type: String, required: true },
    encargado: { type: String, require: true },
    nivel_cuidado: { type: Number, required: true },
    estado: { type: Number, default: 1 },
    fecha_registro: { type: Date, default: new Date() },

})

module.exports = mongoose.model('proyectos', proyectos);