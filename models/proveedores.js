const mongoose = require('mongoose');

const proveedores = new mongoose.Schema({
    nombre: { type: String, required: true },
    ruc: { type: String },
    descripcion: { type: String },
    tipo: { type: String },
    estado: { type: Number, default: 1 },
    fecha_registro: { type: Date, default: new Date() }
})

module.exports = mongoose.model('proveedores', proveedores);