const mongoose = require('mongoose');

const clientes = new mongoose.Schema({
    nombres: { type: String, required: true },
    apellidos: { type: String, required: true },
    ruc: { type: String },
    email: { type: String },
    telefono: { type: Number },
    direccion: { type: String },
    estado: { type: Number, default: 1 },
    fecha_registro: { type: Date, default: new Date() }
})

module.exports = mongoose.model('clientes', clientes);