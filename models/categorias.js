const mongoose = require('mongoose');

const categorias = new mongoose.Schema({
    nombre: { type: String, required: true },
    descripcion: { type: String },
    estado: { type: Number, default: 1 },
})
module.exports = mongoose.model('categorias', categorias);