const mongoose = require('mongoose');

const usuarios = new mongoose.Schema({
    nombres: { type: String, required: true },
    apellidos: { type: String, required: true },
    email: { type: String, required: true },
    telefono: { type: String, required: true },
    empresa: { type: String },
    password: { type: String, required: true },
    sockets: { type: Array, default: [] },
    estado: { type: Number, default: 1 },
    fecha_registro: { type: Date, default: new Date() }
})

module.exports = mongoose.model('usuarios', usuarios);