const mongoose = require('mongoose');

const productos = new mongoose.Schema({
    categoria_id: { type: String, required: true },
    nombre: { type: String, required: true },
    descripcion: { type: String },
    precio: { type: Number, required: true },
    moneda: { type: String, required: true },
    unidad: { type: String, required: true },
    estado: { type: Number, default: 1 },
    fecha_modificacion: { type: Date, default: new Date() }
})

module.exports = mongoose.model('productos', productos);