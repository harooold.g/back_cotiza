const usuariosController = require('../controllers/usuarios');
const cotizacionesController = require('../controllers/cotizaciones');

module.exports = function(io) {

    io.on('connection', (socket) => {
        socket.on('loginUsuario', (data, callback) => {
            socket.username = data.user_id;
            usuariosController.updateSocket(data.user_id, socket.id).then(
                (success) => {
                    callback({ success: true, message: 'OK' })
                },
                (err) => {
                    callback({ success: false, message: 'ERROR' })
                }
            )
        })

        socket.on('registrarCotizacion', (data, callback) => {
            cotizacionesController.store(data.cliente_id, data.orden).then(
                (success) => {
                    io.emit('nuevaCotizacion', success);
                    callback({ success: true, message: 'OK' })
                },
                (err) => {
                    callback({ success: false, message: 'ERROR' })
                }
            )
        })

        socket.on('logoutUsuario', (callback) => {
            usuariosController.disconnectSocket(socket.username, socket.id).then(
                (success) => {
                    callback({ success: true, message: 'OK' })
                },
                (err) => {
                    callback({ success: false, message: 'ERROR' })
                }
            )
        })

        socket.on('disconnect', () => {
            usuariosController.disconnectSocket(socket.username, socket.id).then(
                (success) => {},
                (err) => {}
            )
        })

    })

}