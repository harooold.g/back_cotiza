const clientesModels = require('../models/clientes');

const clientes = {

    show(cliente_id) {
        return new Promise((resolve, reject) => {

            clientesModels.findOne({ _id: cliente_id }, { nombres: true, apellidos: true, email: true, direccion: true, telefono: true }).then(
                (rowCliente) => {
                    if (!rowCliente) reject({ message: 'Cliente no existe' });
                    resolve(rowCliente);
                },
                (error) => {
                    reject(error);
                }
            )
        });
    },

    store(nombres, apellidos, ruc, email, direccion, telefono) {

        return new Promise((resolve, reject) => {
            clientesModels.create({ nombres, apellidos, ruc, email, direccion, telefono }).then(
                (newCliente) => {
                    resolve(newCliente)
                },
                (error) => {
                    reject(error);
                }
            )
        });
    },

    list() {
        return new Promise((resolve, reject) => {
            clientesModels.find().then(
                (listCliente) => {
                    resolve(listCliente);
                },
                (error) => {
                    reject(error);
                }
            )
        });
    },

    delete(cliente_id) {
        return new Promise((resolve, reject) => {
            clientesModels.deleteOne({ _id: cliente_id }).then(
                (result) => {
                    resolve(result);
                },
                (error) => {
                    reject(error);
                }
            )
        });
    },

    update(cliente_id, nombres, apellidos, ruc, email, direccion, telefono) {
        return new Promise((resolve, reject) => {
            let _set = {
                nombres,
                apellidos,
                ruc,
                email,
                direccion,
                telefono
            }

            clientesModels.updateOne({ _id: cliente_id }, { $set: _set }).then(
                (result) => {
                    if (!result.n) reject({ message: 'Cliente no existe' })
                    resolve(result);
                },
                (error) => {
                    reject(error);
                }
            )
        });
    }
}

module.exports = clientes;