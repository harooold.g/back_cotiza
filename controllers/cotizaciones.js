const cotizacionesModels = require("../models/cotizaciones");
const clientesModels = require("../models/clientes");
const _ = require("lodash");
var yhead = 210;
var yrow = 225;
var subtotal = 0;
var igv = 0;
var total = 0;
const mycoti = {};
const cotizaciones = {
  list() {
    return new Promise((resolve, reject) => {
      cotizacionesModels.find().then(
        (listCotizaciones) => {
          resolve(listCotizaciones);
        },
        (error) => {
          reject(error);
        }
      );
    });
  },

  store(cliente, direccion, email, telefono, numero, trabajo, orden) {
    return new Promise((resolve, reject) => {
      cotizacionesModels
        .create({
          cliente,
          direccion,
          email,
          total: _.sumBy(orden, (x) => x.parcial),
          telefono,
          numero,
          trabajo,
          orden,
          fecha_registro: new Date(),
        })
        .then(
          (newCotizacion) => {
            console.log("NEW COTIZACION");
            makepdf(newCotizacion);
            resolve(newCotizacion);
          },
          (error) => {
            reject(error);
          }
        );
    });
  },

  show(cotizacion_id) {
    return new Promise((resolve, reject) => {
      cotizacionesModels.findOne({ _id: cotizacion_id }).then(
        (rowCoti) => {
          if (!rowCoti) reject({ message: "Cotizacion no existe" });
          resolve(rowCoti);
        },
        (error) => {
          reject(error);
        }
      );
    });
  },

  delete(cotizacion_id) {
    return new Promise((resolve, reject) => {
      cotizacionesModels.deleteOne({ _id: cotizacion_id }).then(
        (result) => {
          resolve(result);
        },
        (error) => {
          reject(error);
        }
      );
    });
  },

  update(
    cotizacion_id,
    cliente,
    direccion,
    email,
    telefono,
    numero,
    trabajo,
    orden
  ) {
    return new Promise((resolve, reject) => {
      let _set = {
        cliente,
        direccion,
        email,
        telefono,
        numero,
        trabajo,
        orden,
      };

      cotizacionesModels.updateOne({ _id: cotizacion_id }, { $set: _set }).then(
        (result) => {
          if (!result.n) reject({ message: "Cotizacion no existe" });
          resolve(result);
        },
        (error) => {
          reject(error);
        }
      );
    });
  },
};

function makepdf(newCotizacion) {
  try {
    this.mycoti = newCotizacion;

    const doc = new PDFDocument();
    doc.pipe(fs.createWriteStream(`./public/pdf/${newCotizacion.numero}.pdf`));

    generateHeader(doc);
    generateClienteinfo(doc);

    doc.font("Times-Bold", 9);
    generateTableHead(
      doc,
      yhead,
      "Item",
      "Trabajo a ejecutar",
      "Und",
      "Cantidad",
      "P/unitario",
      "Total"
    );

    doc
      .strokeColor("#aaaaaa")
      .lineWidth(1)
      .moveTo(45, 221)
      .lineTo(540, 221)
      .stroke();

    doc.font("Times-Roman", 8);

    for (const item of newCotizacion.orden) {
      console.log(item);
      subtotal = subtotal + item.parcial;
      generateTableRow(
        doc,
        yrow,
        item.tipo.toUpperCase(),
        item.categoria.toUpperCase(),
        item.nombre.toUpperCase(),
        item.descripcion.toUpperCase(),
        item.unidad,
        item.cantidad,
        item.precio.toFixed(2),
        item.parcial.toFixed(2)
      );
      yrow = yrow + 30;
    }

    doc
      .strokeColor("#aaaaaa")
      .lineWidth(1)
      .moveTo(45, yrow)
      .lineTo(540, yrow)
      .stroke();

    igv = 0.18 * subtotal;

    total = subtotal + igv;

    yrow = yrow + 5;
    igv = igv.toFixed(2);
    subtotal = subtotal.toFixed(2);
    total = total.toFixed(2);

    generateTotal(doc, yrow);

    yrow = yrow + 35;

    generateFooter(doc, yrow);
    doc.end();
  } catch (error) {
    console.log(error);
  }
}

function generateHeader(doc) {
  let now = new Date();
  var months = [
    "Enero",
    "Febrero",
    "Marzo",
    "Abril",
    "Mayo",
    "Junio",
    "Julio",
    "Agosto",
    "Septiembre",
    "Octubre",
    "Noviembre",
    "Diciembre",
  ];
  doc
    .image("./public/images/logowhd.jpg", 50, 40, { width: 60 })
    .fillColor("#444444")
    .fontSize(8)
    .text("comercial@whdeco.com", 0, 75, {
      align: "right",
    })
    .text(
      `Surco, ${now.getDate()} de ${
        months[now.getMonth()]
      } del ${now.getFullYear()}`,
      0,
      90,
      {
        align: "right",
      }
    )
    .moveDown();
}

function generateClienteinfo(doc) {
  let now = new Date();
  doc
    .font("Times-Bold", 13)
    .text(
      `COTIZACION: Nº ${now.getFullYear()}-${this.mycoti.numero}`,
      50,
      110,
      {
        align: "center",
      }
    );
  doc
    .font("Times-Bold", 9)
    .text(`Cliente:`, 50, 128, {
      align: "left",
    })
    .text(`Direccion:`, 50, 163, { align: "left" })
    .text(`Email:`, 50, 145, {
      align: "left",
    })
    .text(`Trabajo a realizar:`, 50, 181, {
      align: "left",
    })
    .font("Times-Roman", 9)
    .text(`${this.mycoti.cliente}`, 110, 128, {
      align: "left",
    })
    .text(`${this.mycoti.direccion}`, 110, 163, { align: "left" })
    .text(`${this.mycoti.email}`, 110, 145, {
      align: "left",
    })
    .text(`${this.mycoti.trabajo}`, 130, 181, {
      align: "left",
    });
}

function generateTableHead(
  doc,
  y,
  item,
  descripcion,
  unidad,
  cantidad,
  punitario,
  total
) {
  doc
    .text(item, 50, y)
    .text(descripcion, 120, y, {})
    .text(unidad, 300, y, { width: 50, align: "right" })
    .text(cantidad, 350, y, { width: 60, align: "right" })
    .text(punitario, 410, y, { width: 60, align: "right" })
    .text(total, 460, y, { width: 60, align: "right" });
}

function generateTableRow(
  doc,
  y,
  item,
  categoria,
  nombre,
  descripcion,
  unidad,
  cantidad,
  punitario,
  total
) {
  doc
    .text(item, 50, y)
    .text(categoria, 120, y, {})
    .text(nombre, 120, y + 10, {})
    .text(descripcion, 120, y + 20, {})
    .text(unidad, 300, y, { width: 50, align: "center" })
    .text(cantidad, 350, y, { width: 60, align: "center" })
    .text(`S/. ${punitario}`, 410, y, { width: 60, align: "right" })
    .text(`S/. ${total}`, 470, y, { width: 60, align: "right" });
}

function generateTotal(doc, y) {
  doc
    .text("Sub Total", 410, y, { width: 60, align: "right" })
    .text("IGV 18%", 410, y + 10, { width: 60, align: "right" })
    .text("TOTAL", 410, y + 20, { width: 60, align: "right" })
    .text(`S/. ${subtotal}`, 470, y, { width: 60, align: "right" })
    .text(`S/. ${igv}`, 470, y + 10, { width: 60, align: "right" })
    .text(`S/. ${total}`, 470, y + 20, { width: 60, align: "right" });
}

function generateFooter(doc, y) {
  doc
    .font("Times-Bold", 9)
    .text(`Condiciones:`, 50, y, {
      align: "left",
    })
    .text(`Validez de oferta:`, 50, y + 30, {
      align: "left",
    })
    .text(`Tiempo de entrega:`, 50, y + 60, {
      align: "left",
    })
    .text(`Pagos:`, 50, y + 90, {
      align: "left",
    })
    .text(`N° Cuenta Corriente`, 380, y + 90, {
      align: "left",
    })
    .text(`BBVA`, 410, y + 103, {
      align: "left",
    })
    .text(`BCP`, 410, y + 113, {
      align: "left",
    })
    .font("Times-Roman", 9)
    .text(`0011-0150-040100079670`, 440, y + 103, {
      align: "left",
    })
    .text(`194-26496140-76`, 440, y + 113, {
      align: "left",
    })
    .text(`Garantia de producto 3 años`, 120, y + 10, {
      align: "left",
    })
    .text(`Validez de oferta:`, 120, y + 40, {
      align: "left",
    })
    .text(`Según cronograma`, 120, y + 70, {
      align: "left",
    })
    .text(`80% a la aceptacion del Contrato`, 120, y + 100, {
      align: "left",
    })
    .text(`20% al termino de la obra`, 120, y + 110, {
      align: "left",
    })
    .text(`Atentamente`, 120, y + 130, {
      align: "left",
    })
    .text(`Kathlen Romero Sabas`, 120, y + 150, {
      align: "left",
    })
    .text(`Gerente General`, 120, y + 160, {
      align: "left",
    });
  doc
    .lineWidth(1)
    .moveTo(60, y + 180)
    .lineTo(540, y + 180)
    .stroke();
  doc
    .font("Times-Bold", 9)
    .text(`WORLD HOUSE DECO SAC`, 50, y + 190, {
      align: "center",
    })
    .font("Times-Roman", 9)
    .text(`Av. Tomas Marsano Nro. 2875 Santiago de Surco - Lima`, 50, y + 200, {
      align: "center",
    });
}

module.exports = cotizaciones;
