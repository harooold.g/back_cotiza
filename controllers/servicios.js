const categoriasModels = require('../models/categorias');
const serviciosModels = require('../models/servicios');

const servicios = {
    show(servicios_id) {
        return new Promise((resolve, reject) => {
            serviciosModels.findOne({ _id: servicios_id }, { categoria_id: true, nombre: true, precio: true, descripcion: true, moneda: true, unidad: true }).then(
                (rowServicio) => {
                    categoriasModels.populate(rowServicio, { path: 'categorias_id', select: '_id nombre descripcion' }).then(
                        (rowAll) => {
                            if (!rowAll) reject({ message: 'producto no existe' });
                            resolve(rowAll);
                        },
                        (error) => {
                            reject(error);
                        }
                    )
                },
                (error) => {
                    reject(error);
                }
            )
        });
    },

    store(categoria_id, nombre, descripcion, precio, moneda, unidad) {
        return new Promise((resolve, reject) => {
            serviciosModels.create({
                categoria_id,
                nombre,
                descripcion,
                precio,
                moneda,
                unidad
            }).then(
                (rowServicio) => {
                    resolve({
                        _id: rowServicio.id,
                        nombre: rowServicio.nombre,
                        descripcion: rowServicio.descripcion,
                        precio: rowServicio.precio,
                        moneda: rowServicio.moneda,
                        unidad: rowServicio.unidad
                    })
                },
                (error) => {
                    reject(error);
                }
            )
        });
    },

    list() {
        return new Promise((resolve, reject) => {
            serviciosModels.find({}, { _id: true, categoria_id: true, nombre: true, precio: true, descripcion: true, moneda: true, unidad: true }).then(
                (listServicio) => {
                    categoriasModels.populate(listServicio, { path: 'categorias_id', select: '_id nombre descripcion' }).then(
                        (rowAll) => {
                            if (!rowAll) reject({ message: 'producto no existe' });
                            resolve(rowAll);
                        },
                        (error) => {
                            reject(error);
                        }
                    )
                },
                (error) => {
                    reject(error);
                }
            )
        });
    },

    delete(servicios_id) {

        return new Promise((resolve, reject) => {
            serviciosModels.deleteOne({ _id: servicios_id }).then(
                (result) => {
                    resolve(result);
                },
                (error) => {
                    reject(error);
                }
            )
        });
    },

    update(servicios_id, categoria_id, nombre, descripcion, precio, moneda, unidad) {
        return new Promise((resolve, reject) => {
            let _set = {
                categoria_id,
                nombre,
                descripcion,
                precio,
                moneda,
                unidad
            }

            serviciosModels.updateOne({ _id: servicios_id }, { $set: _set }).then(
                (result) => {
                    if (!result.n) reject({ message: 'El producto no existe' });
                    resolve(result);
                },
                (error) => {
                    reject(error);
                }
            )
        });
    },

    listByCategoria(categoria_id) {
        return new Promise((resolve, reject) => {
            serviciosModels.find({ categoria_id: categoria_id }, { _id: true, categoria_id: true, nombre: true, descripcion: true, precio: true, moneda: true, unidad: true }).then(
                (listServicio) => {
                    categoriasModels.populate(listServicio, { path: 'categorias_id', select: '_id nombre descripcion' }).then(
                        (rowAll) => {
                            resolve(rowAll);
                        },
                        (error) => {
                            reject(error);
                        }
                    )
                },
                (error) => {
                    reject(error);
                }
            )
        });
    }
}

module.exports = servicios;