const categoriasModels = require("../models/categorias");
const productosModels = require("../models/productos");

const productos = {
  show(productos_id) {
    return new Promise((resolve, reject) => {
      productosModels
        .findOne(
          { _id: productos_id },
          {
            categoria_id: true,
            nombre: true,
            precio: true,
            descripcion: true,
            moneda: true,
            unidad: true,
          }
        )
        .then(
          (rowProducto) => {
            categoriasModels
              .populate(rowProducto, {
                path: "categoria_id",
                select: "_id nombre descripcion",
              })
              .then(
                (rowAll) => {
                  if (!rowAll) reject({ message: "producto no existe" });
                  resolve(rowAll);
                },
                (error) => {
                  reject(error);
                }
              );
          },
          (error) => {
            reject(error);
          }
        );
    });
  },

  store(categoria_id, nombre, descripcion, precio, moneda, unidad) {
    return new Promise((resolve, reject) => {
      productosModels
        .create({
          categoria_id,
          nombre,
          descripcion,
          precio,
          moneda,
          unidad,
        })
        .then(
          (rowProducto) => {
            resolve({
              _id: rowProducto.id,
              nombre: rowProducto.nombre,
              descripcion: rowProducto.descripcion,
              precio: rowProducto.precio,
              moneda: rowProducto.moneda,
              unidad: rowProducto.unidad,
            });
          },
          (error) => {
            reject(error);
          }
        );
    });
  },

  list() {
    return new Promise((resolve, reject) => {
      productosModels
        .find(
          {},
          {
            _id: true,
            categoria_id: true,
            nombre: true,
            precio: true,
            descripcion: true,
            moneda: true,
            unidad: true,
          }
        )
        .then(
          (listProducto) => {
            categoriasModels
              .populate(listProducto, {
                path: "categoria_id",
                select: "_id nombre descripcion",
              })
              .then(
                (rowAll) => {
                  if (!rowAll) reject({ message: "producto no existe" });
                  resolve(rowAll);
                },
                (error) => {
                  reject(error);
                }
              );
          },
          (error) => {
            reject(error);
          }
        );
    });
  },

  delete(productos_id) {
    console.log(productos_id);
    return new Promise((resolve, reject) => {
      productosModels.deleteOne({ _id: productos_id }).then(
        (result) => {
          resolve(result);
        },
        (error) => {
          reject(error);
        }
      );
    });
  },

  update(
    productos_id,
    categoria_id,
    nombre,
    descripcion,
    precio,
    moneda,
    unidad
  ) {
    return new Promise((resolve, reject) => {
      let _set = {
        categoria_id,
        nombre,
        descripcion,
        precio,
        moneda,
        unidad,
      };

      productosModels.updateOne({ _id: productos_id }, { $set: _set }).then(
        (result) => {
          if (!result.n) reject({ message: "El producto no existe" });
          resolve(result);
        },
        (error) => {
          reject(error);
        }
      );
    });
  },

  listByCategoria(categoria_id) {
    return new Promise((resolve, reject) => {
      productosModels
        .find(
          { categoria_id: categoria_id },
          {
            _id: true,
            categoria_id: true,
            nombre: true,
            descripcion: true,
            precio: true,
            moneda: true,
          }
        )
        .then(
          (listProducto) => {
            categoriasModels
              .populate(listProducto, {
                path: "categorias_id",
                select: "_id nombre descripcion",
              })
              .then(
                (rowAll) => {
                  resolve(rowAll);
                },
                (error) => {
                  reject(error);
                }
              );
          },
          (error) => {
            reject(error);
          }
        );
    });
  },
};

module.exports = productos;
