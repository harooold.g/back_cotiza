const categoriasModels = require("../models/categorias");

const categorias = {
  show(categoria_id) {
    return new Promise((resolve, reject) => {
      categoriasModels.findOne({ _id: categoria_id }).then(
        (rowCategoria) => {
          if (!rowCategoria) reject({ message: "Categoria no existe" });
          resolve(rowCategoria);
        },
        (error) => {
          reject(error);
        }
      );
    });
  },

  store(nombre, descripcion) {
    return new Promise((resolve, reject) => {
      categoriasModels.create({ nombre, descripcion }).then(
        (newCategoria) => {
          resolve(newCategoria);
        },
        (error) => {
          reject(error);
        }
      );
    });
  },

  list() {
    return new Promise((resolve, reject) => {
      categoriasModels.find().then(
        (listCategoria) => {
          resolve(listCategoria);
        },
        (error) => {
          reject(error);
        }
      );
    });
  },

  delete(categoria_id) {
    return new Promise((resolve, reject) => {
      categoriasModels.deleteOne({ _id: categoria_id }).then(
        (result) => {
          resolve(result);
        },
        (error) => {
          reject(error);
        }
      );
    });
  },

  update(categoria_id, nombre, descripcion) {
    return new Promise((resolve, reject) => {
      let _set = {
        nombre,
        descripcion,
      };

      categoriasModels.updateOne({ _id: categoria_id }, { $set: _set }).then(
        (result) => {
          if (!result.n) reject({ message: "Categoria no existe" });
          resolve(result);
        },
        (error) => {
          reject(error);
        }
      );
    });
  },
};

module.exports = categorias;
